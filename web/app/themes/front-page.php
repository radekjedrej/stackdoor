<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package stackdoor
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

			<?php get_template_part( 'template-parts/blocks/main-banner' ); ?>
			<?php get_template_part( 'template-parts/blocks/product-range' ); ?>
			<?php get_template_part( 'template-parts/blocks/stack-in-action' ); ?>
			<?php get_template_part( 'template-parts/blocks/patented-stackdoor' ); ?>
			<?php get_template_part( 'template-parts/blocks/signup' ); ?>
			<?php get_template_part( 'template-parts/blocks/contact' ); ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php

get_footer();
