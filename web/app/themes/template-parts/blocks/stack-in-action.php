

<section class="stack-in-action" id="features">
    <div class="container stack-in-action-header">
        <div class="row stack-in-action-row">
            <div class="col-sm-6 col-md-3 text-center box-action">
                <div class="stack-in-action-icon">
                    <img class="img-fluid" src="<?php home_url(); ?>/app/themes/stackdoor/assets/img/icon1.svg" alt="Stackdoor">
                </div>
                <div class="stack-in-action-text">
                    20m wide single <br/>span
                </div>
            </div>
            <div class="col-sm-6 col-md-3 text-center box-action">
                <div class="stack-in-action-icon">
                    <img class="img-fluid" src="<?php home_url(); ?>/app/themes/stackdoor/assets/img/icon2.svg" alt="Stackdoor">
                </div>
                <div class="stack-in-action-text">
                    Over 80% free <br/>open area
                </div>
            </div>
            <div class="col-sm-6 col-md-3 text-center box-action">
                <div class="stack-in-action-icon">
                    <img class="img-fluid" src="<?php home_url(); ?>/app/themes/stackdoor/assets/img/image3.svg" alt="Stackdoor">
                </div>
                <div class="stack-in-action-text">
                    Self-locking system <br/>to prevent lifting 
                </div>
            </div>
            <div class="col-sm-6 col-md-3 text-center box-action">
                <div class="stack-in-action-icon">
                    <img class="img-fluid" src="<?php home_url(); ?>/app/themes/stackdoor/assets/img/image4.svg" alt="Stackdoor">
                </div>
                <div class="stack-in-action-text">
                    Small stacking area <br/> at high level
                </div>
            </div>
        </div>
    </div>
</section>

    <div class="main-gallery" id="gallery">

        <div class="stack-in-action-gallery container-fluid px-0">
            <div class="row">
                <?php
                $photos = get_field('photos');
                ?>

                <?php foreach($photos as $photo): ?>
                    <div class="col-6 col-md-4 px-0">
                        <img src="<?php echo $photo['image'] ['url']; ?>" alt="<?php echo $photo['image'] ['alt']; ?>" class="img-fluid">
                    </div>
                <?php endforeach; ?>
            </div>
        </div>

        <div class="gallery-banner">
            <h1 class="text-uppercase"><span class="blue">stackdoor</span>  in action</h1>
        </div>

    </div>
