
<section class="patented-stackdoor" id="benefits">
    <div class="container patented-container">
        <div class="patented-title">
            <h1 class="text-center text-uppercase">What makes the patented <span class="blue">stackdoor</span> unique?</h1>
        </div>
        <div class="patented-text text-center">
            The inventive pin-to-profile technology and chain drive system provides self-locking, requires no auxiliary locks and prevents lifting. Stackdoor creates a seamless security solution, perfect for applications including car park entrances, retail outlets and low-to-medium-risk commercial buildings.
        </div>
        <div class="row">

            <?php
                $usps = get_field('usps');
            ?>

            <?php foreach($usps as $usp): ?>
                <div class="col-md-6 px-md-1 patented-box">
                    <div class="patented-box-inner text-center" style="background-color:<?php echo $usp['bg-color']; ?>">
                        <div class="box-icon">
                            <img src="<?php echo $usp['icon'] ['url']; ?>" alt="<?php echo $usp['icon'] ['alt']; ?>" class="img-fluid">
                        </div>
                        <div class="box-title">
                            <?php echo $usp['title']; ?>
                        </div>
                        <div class="box-text">
                            <?php echo $usp['content']; ?>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>

        </div>
    </div>
</section>
