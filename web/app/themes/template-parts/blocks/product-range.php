<section class="product-range" id="range">
    <div class="container">
        <div class="product-range-title text-center">
            <h1 class="text-uppercase">explore the <span class="blue">stackdoor</span> product range</h1>
        </div>
        <div class="row">
            <?php
                $products = get_field('products');
            ?>

            <?php foreach($products as $product): ?>
                <div class="col-sm-6 col-lg-4">
                    <div class="box-image">
                        <img src="<?php echo $product['image'] ['url']; ?>" alt="<?php echo $product['image'] ['alt']; ?>" class="img-fluid">
                    </div>
                    <div class="box-content">
                        <div class="box-title text-uppercase text-center">
                            <?php echo $product['title']; ?>
                        </div>
                        <div class="box-text">
                            <?php echo $product['content']; ?>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>

        <div class="text-center call-us">
            <div class="call-us-button text-uppercase">
                call today on <span class="nobr"><?php the_field('phone', 'option') ?></span>
            </div>
        </div>

    </div>
</section>


