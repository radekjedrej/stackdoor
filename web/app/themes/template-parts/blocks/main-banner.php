<div class="section main-banner">
    <div class="main-banner-image">

        <?php
        $banners = get_field('banners');
        ?>

        <div class="carousel">
            <?php foreach($banners as $banner): ?>
                <div class="slide">
                    <img src="<?php echo $banner['image'] ['url']; ?>" alt="<?php echo $banner['image'] ['alt']; ?>">
                </div>
            <?php endforeach; ?>
        </div>

        <div class="title-wrapper">
            <h1>Introducing a revolutionary new product to the security market</h1>
        </div>

    </div>

    <div class="second-banner-image d-flex" id="what">
        <div class="left-side d-none d-lg-block">
            <?php stackdoor_post_thumbnail(); ?>
        </div>
        <div class="right-side">
            <div class="text-wrapper">
                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                    <?php the_content(); ?>
                <?php endwhile; endif; ?>

            </div>
        </div>
    </div>

</div>