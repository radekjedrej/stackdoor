<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package stackdoor
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<link rel="stylesheet" href="https://use.typekit.net/vyf7cvf.css">
	 <link href="https://fonts.googleapis.com/css?family=Rajdhani:400,500,600,700" rel="stylesheet"> 

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'stackdoor' ); ?></a>

	<header id="masthead" class="site-header">
	<section class="top-nav">
        <nav class="navbar navbar-expand-lg bg-black py-3 px-2">
            <div class="container navigation-container">
                <button class="navbar-toggler" data-toggle="collapse" data-target="#navbarNav">
                    <span class="navbar-toggler-icon">
                        <img src="<?php home_url(); ?>/app/themes/stackdoor/assets/img/burger.png" alt="Stackdoor Logo" class="img-fluid">
                    </span>
                </button>
                <a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="nav-link gray d-block d-lg-none stackdoor-logo">
                    <img src="<?php home_url(); ?>/app/themes/stackdoor/assets/img/logo3.png" alt="Stackdoor Logo" class="img-fluid">
                </a>
                
                <div class="collapse navbar-collapse justify-content-center" id="navbarNav">
                    <ul class="navbar-nav text-uppercase">
                        <li class="nav-item stackdoor-logo">
                            <a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="nav-link gray">
                                <img src="<?php home_url(); ?>/app/themes/stackdoor/assets/img/logo3.png" alt="Stackdoor Logo" class="img-fluid d-none d-lg-block">
                            </a>
                        </li>
                        <li class="nav-item extra-padding-top">
                            <a href="<?php echo esc_url( home_url( '/' ) ); ?>#what" class="nav-link gray">What is stackdoor</a>
						</li>
						<span class="d-none d-lg-block space extra-padding-top">
							/
						</span>
                        <li class="nav-item extra-padding-top">
                            <a href="<?php echo esc_url( home_url( '/' ) ); ?>#range" class="nav-link gray">The stackdoor range</a>
						</li>
						<span class="d-none d-lg-block space extra-padding-top">
							/
						</span>
						 <li class="nav-item extra-padding-top">
                            <a href="<?php echo esc_url( home_url( '/' ) ); ?>#features" class="nav-link gray">Key features</a>
						</li>
						<span class="d-none d-lg-block space extra-padding-top">
							/
						</span> 
						 <li class="nav-item extra-padding-top">
                            <a href="<?php echo esc_url( home_url( '/' ) ); ?>#gallery" class="nav-link gray">Gallery</a>
						</li>
						<span class="d-none d-lg-block space extra-padding-top">
							/
						</span>
						<li class="nav-item extra-padding-top">
                            <a href="<?php echo esc_url( home_url( '/' ) ); ?>#benefits" class="nav-link gray">The Key Benefits</a>
						</li>
						<span class="d-none d-lg-block space extra-padding-top">
							/
						</span>
						<li class="nav-item extra-padding-top">
                            <a href="<?php echo esc_url( home_url( '/' ) ); ?>#getintouch" class="nav-link gray">Get In touch</a>
                        </li>
                    </ul>
                </div>
            </div>
		</nav>
	</section>
	<section class="gray-info">
		<div class="container container-grey-info text-center text-lg-right text-uppercase">
			stackdoor is manufactured in the uk by <span class="blue">cgt security ltd</span> Call today on <span class="blue nobr"><?php the_field('phone', 'option') ?></span>
		</div>	
	</section>
	</header><!-- #masthead -->

	<div id="content" class="site-content">
