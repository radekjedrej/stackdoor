<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package stackdoor
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer">
		<div class="footer-dark-strip">
			<div class="container text-center text-uppercase">
				<span class="nobr"><img class="img-fluid" src="<?php home_url(); ?>/app/themes/stackdoor/assets/img/phone-icon.svg" alt="Stackdoor">Call CGT SECURITY LTD TODAY <span class="blue"><?php the_field('phone', 'option') ?></span></span>
				<span class="nobr"><img class="img-fluid margin-left" src="<?php home_url(); ?>/app/themes/stackdoor/assets/img/letter-icon.svg" alt="Stackdoor">Email us at  <span class="blue"><a href="mailto:<?php the_field('email', 'option') ?>"><?php the_field('email', 'option') ?></a></span></span>
			</div>
		</div>
		<div class="footer-blue">
			<div class="container text-center text-uppercase">
				<p>CGT Security Ltd ARE A leading manufacturer of innovative shutters.</p>
				<a href="https://cgtsecurity.com/"><img class="img-fluid" src="<?php home_url(); ?>/app/themes/stackdoor/assets/img/cgt-logo.svg" alt="Stackdoor" style="width: 300px;"></a>
			</div>
		</div>

		<div class="container">
			<div class="row">
				<div class="col-12 privacy-cookie text-center">
					<a href="<?php echo home_url()?>/privacy-policy">Privacy Policy</a> |
					<a href="<?php echo home_url()?>/cookie-policy"> Cookie Policy</a>
				</div>
			</div>
			<div class="row">
				<div class="col-12 text-center mb-4">
					&copy; Copyright CGT Security <?php echo date("Y"); ?>
				</div>
			</div>
		</div>

	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
