<?php
function stackdoor_assets() {
    wp_enqueue_style( 'style', get_template_directory_uri() . '/dist/css/main.css',false,'1.1','all');

    /**
     * Load our IE-only stylesheet for all versions of IE:
     * <!--[if IE]> ... <![endif]-->
     */
    wp_enqueue_style( 'stackdoor-ie', get_template_directory_uri() . '/dist/css/ie.css',false,'1.1','all');
    wp_style_add_data( 'stackdoor-ie', 'conditional', 'IE' );

    // bootstrap styles
    wp_enqueue_style( 'bootstrap', get_stylesheet_directory_uri() . '/bootstrap/bootstrap.min.css', array(), 20141119 );
    // bootstrap scripts
    wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/bootstrap/bootstrap.min.js', array('jquery'), '20120206', true );

    wp_enqueue_script( 'jquery' );

    wp_register_script( 'plugins', get_template_directory_uri() . '/dist/js/all.min.js', array ( 'jquery' ), 1.1, true);
    wp_enqueue_script( 'owl.carousel.min', get_template_directory_uri() . '/js/owl.carousel.min.js', array ( 'jquery' ), 1.1, true);

    // Localize the script with new data
    $theme_path_array = array(
        'theme_path' => get_template_directory_uri()
    );
    wp_localize_script( 'plugins', 'carousel_nav', $theme_path_array );

    wp_enqueue_script('plugins');
};

add_action( 'wp_enqueue_scripts', 'stackdoor_assets' );